const fs = require('fs');
const path = require('path');

exports.getDirectoryListing = (req, res) => {
  const encodedPath = req.params.path;
  const decodedPath = decodeURIComponent(encodedPath);


  fs.readdir(directoryPath, { withFileTypes: true }, function (err, files) {
    if (err) {
      console.error('Unable to scan directory:', err);
      return res.status(500).json({ error: 'Unable to scan directory' });
    }
    const directoryListing = files.map((file) => {
      const filePath = path.join(directoryPath, file.name);
      const stats = fs.statSync(filePath);

      return {
        filename: file.name,
        directoryPath: filePath,
        size: stats.size,
        extension: path.extname(file.name),
        createdDate: stats.ctime.toISOString(),
        permissions: stats.mode,
        type: file.isDirectory() ? 'directory' : 'file',
      };
    });

    return res.json(directoryListing);
  });
};

exports.getHealth = (req, res) => {
  res.send('I am alive and healthy');
};
