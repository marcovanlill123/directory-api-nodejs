const express = require('express');
const router = express.Router();
const directoryController = require('../controllers/directoryController');

//ENDPOINTS

router.get('/directory/:path', directoryController.getDirectoryListing);
router.get('/health', directoryController.getHealth);

module.exports = router;
