const express = require('express');
const directoryRoutes = require('./api/routes/directoryRoutes');
const cors = require('cors');
const app = express();
app.use(cors());

app.use('/', directoryRoutes);

app.listen(8080, () => {
  console.log('Server is running on port 8080');
});